<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStayeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stayees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('booking');
            $table->string('room');
            $table->string('name');
            $table->string('gender');
            $table->string('type'); //adult , child
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stayees');
    }
}
