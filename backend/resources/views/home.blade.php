@extends('layouts.app')

@section('title',"Home")
@section('content')
<div class="main jumbotron bg-white mb-0">
    <div class="container text-black-50">
        <div class="py-4"></div>
        <h1 class="text-thin  main-font">The Best Service Apartments.<br>Best In Trivandrum.</h1>
        <p>Unwind the clock of modern life. Unlock the door to a wonder of the world.</p>
        <div class="py-2"></div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-4 bg-primary text-white py-4">
            <div class="container p-4">
                <div class="py-2"></div>
                <h2>Single BHk</h2>
                <p>From</p>
                <del>Rs.3500/-</del>
                <h3>Rs. 1,500*</h3>
                <div class="py-2"></div>
                <a href="http://#" class="btn btn-outline-white border-radius-0">More Details <icon-item type="material" class="">arrow_forward</icon-item></a>
                <a href="{{route('apartments.index')}}" class="btn btn-white border-radius-0">All Apartments <icon-item type="material" class="">arrow_forward</icon-item></a>
            </div>
        </div>
        <div class="col-sm-12 col-md-8 bg-white doppleg" style="background-image:url({{asset('storage/images/bedroom-416062.jpg')}})"></div>
    </div>
</div>
{{-- Services --}}
<div class="jumbotron mb-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6 col-lg-3 text-center">
                <icon-item type="fa" font-awesome-class="fas fa-wind fa-2x" class="text-info"></icon-item>
                <p class="lead text-thin">Fully Furnished & A/C</p>
                <p>Fully furnished a/c apartments with 1/2/3/4 bedroom options</p>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 text-center">
                <icon-item type="fa" font-awesome-class="fas fa-map-marked-alt fa-2x" class="text-success"></icon-item>
                <p class="lead text-thin">Good Proximity</p>
                <p>Technopark, Infosys, UST Global campuses and 5 min drive to airport</p>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 text-center">
                <icon-item type="fa" font-awesome-class="fas fa-wifi fa-2x" class="text-black"></icon-item>
                <p class="lead text-thin">Free Wifi & TV</p>
                <p>We provide FREE high speed internet via wifi and cable TV</p>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 text-center">
                <icon-item type="fa" font-awesome-class="fas fa-dumbbell fa-2x" class="text-danger"></icon-item>
                <p class="lead text-thin">Gym & Pool</p>
                <p>We provide Gym and Swimming pool for our guests*</p>
            </div>
        </div>
    </div>
</div>
{{-- Services --}}

{{-- Location --}}
<div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-6 bg-white doppleg" style="background-image:url({{asset('storage/images/building-2359975.jpg')}})"></div>
            <div class="col-sm-12 col-md-6 bg-primary text-white py-4">
                <div class="container p-4">
                    <div class="py-2"></div>
                    <h2>About Us</h2>
                    <p class="text-justify">
                        Best Serviced apartments offer a fine mix of a standard , superior and deluxe apartments
                        to rent within mintiues in Technopark main areas .which are larger than hotel rooms, range from spacious Studios to 
                        the ultimate in luxury, two 4 bedroom penthouses with private access.
                    </p>
                    <a href="{{route('about')}}" class="btn btn-outline-white border-radius-0">More on us</a>
                </div>
            </div>
        </div>
    </div>
{{-- Location --}}
@endsection
