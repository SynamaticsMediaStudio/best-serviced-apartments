@extends('layouts.app')

@section('content')
<div class="jumbotron">
    <h1>{{$page->name}}</h1>
</div>
<div class="container">
    {{$page->details}}
</div>
@endsection
