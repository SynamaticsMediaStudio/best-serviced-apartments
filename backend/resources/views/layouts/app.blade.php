<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title',"Page") \ {{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
    <v-app>
        <v-toolbar app flat class="white">
            <v-toolbar-title><a href="{{ url('/') }}"><img src="http://bsa.signaturees.com/wp-content/uploads/2019/02/s.jpg" alt="" style="width:30px"></a></v-toolbar-title>
                <v-spacer></v-spacer>
                <v-toolbar-items class="hidden-sm-and-down">
                    <v-btn flat href="{{route('home')}}">{{ __('Home') }}</v-btn>
                    <v-btn flat href="{{route('about')}}">{{ __('About') }}</v-btn>
                    <v-btn flat href="{{route('apartments.index')}}">{{ __('Apartments') }}</v-btn>
                </v-toolbar-items>
        </v-toolbar>
        <v-content>
        @yield('content')
        </v-content>
        <v-footer >
            <v-container grid-list-md>
                <v-layout row wrap>
                    <v-flex xs12 md6 lg6>
                        <h2 class="text-thin">"We work with organisations to craft immersive customer experiences."</h2>
                    </v-flex>
                    <v-flex xs12 md6 lg6>
                            <h6 class="">BOOK APARTMENT</h6>
                            <p class="text-black-50">Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse nihil, flexitarian Truffaut synth art party deep v chillwave.</p>
                            <div class="py-2"></div>
                            <a href="http://#" class="btn btn-black border-radius-0 btn-sm">Book an Apartment</a>                            
                    </v-flex>
                    <v-flex xs12>
                        <small>&copy; Copyright {{date('Y')}}. All Rights Reserved. Synamatics.</small>
                        <icon-item type="fa" font-awesome-class="fab fa-facebook px-2" class="text-black"></icon-item>
                        <icon-item type="fa" font-awesome-class="fab fa-twitter px-2" class="text-black"></icon-item>
                        <icon-item type="fa" font-awesome-class="fab fa-whatsapp px-2" class="text-black"></icon-item>
                        <icon-item type="fa" font-awesome-class="fab fa-linkedin px-2" class="text-black"></icon-item>                        
                    </v-flex>
                </v-layout>
            </v-container>      
        </v-footer>
    </v-app>
    </div>
</body>
</html>
