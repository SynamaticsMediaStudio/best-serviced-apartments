import Home from '../views/Home'

const routes = [

    {
        path: '/',
        name: 'Home',
        component: Home,
        meta:{
          title:"Home"
        }
      }, 
];

export default routes;